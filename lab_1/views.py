from django.shortcuts import render
import datetime

# Enter your name here
mhs_name = 'Marco Kenata' # TODO Implement this
mhs_birth_year = 1998

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age' : calculate_age(mhs_birth_year)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
   age_year = datetime.datetime.today().year
   return age_year-birth_year